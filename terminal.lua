-- Input/output console
-- Single line input
-- Recieved text is added to the start of the output field (so, newest text is at the top)
-- (There is no way to set the default scroll position of a textarea)
-- line breaks are added automatically at the end of each message.
-- "\f" (form feed) clears the output

local S = rawget(_G, "intllib") and intllib.make_gettext_pair() or function(s) return s end

local function update_formspec(meta,pos)
	local swap = meta:get_int("swap")
	local form_string = 	
	"size[8,10]"..
	default.gui_bg_img..	
	"bgcolor[#00000000;false]"..
	"set_focus[input;true]"..
	"field[0.8,0.5;4.7,1;input;"..S("Input:")..";"..
	minetest.formspec_escape(meta:get_string("input")).."]"..
	"button[0.19,0.19;0.55,1;last;>]"..
	"tooltip[last;"..S("Show YOUR last command (not the last cmd of this terminal)").."]"..
	"button[5.08,0.19;1.4,1;send;"..S("Send").."]"..
	"tooltip[send;"..S("Send your command").."]"..
	"button[6.3,0.19;1.4,1;clear;"..S("Clear").."]"..
	"tooltip[clear;"..S("Clear the output history").."]"..
	"textarea[0.5,1.25;7.5,8.8;output;"..S("Output: (top = new)")..";"..
	minetest.formspec_escape(meta:get_string("output"))..
	"]"..
	"field_close_on_enter[input;false]"..
	-- this is added/removed so that the formspec will update every time
	(swap > 0 and " " or "")..
	"field[0.5,9.5;2.65,1;send_channel;"..S("Send Channel")..":;${send_channel}]"..
	"tooltip[send_channel;"..S("Name of the digiline channel to send messages").."]"..
	"field[3.35,9.5;2.65,1;recv_channel;"..S("Receive Channel")..":;${recv_channel}]"..
	"tooltip[recv_channel;"..S("Name of the digiline channel to send messages").."]"

	if minetest.is_protected(pos, "") then
		local is_shared_use = meta:get_int("toggle_shared_use") == 1
		local is_shared_setup = meta:get_int("toggle_shared_setup") == 1
		form_string = form_string..
		"checkbox[5.8,9;toggle_shared_use;"..S("Share use")..";"..tostring(is_shared_use).."]"..
		"tooltip[toggle_shared_use;"..S("when the terminal is protected: Allows all players to use the terminal").."]"..
		"checkbox[5.8,9.4;toggle_shared_setup;"..S("Share setup")..";"..tostring(is_shared_setup).."]"..
		"tooltip[toggle_shared_setup;"..S("when the terminal is protected: Allows all players to set channel parameters").."]"
	else
		form_string = form_string..
		"label[5.8,9.1;"..S("Free use").."]"..
		"label[5.8,9.5;"..S("Free setup").."]"..
		"tooltip[5.8,9;1,2.3;"..S("Your terminal is free to use & setup by everybody! ... If you dont want :  protect it !")..";#f50f0f;#ecf011]"	
	end

	meta:set_string("formspec",form_string)
	meta:set_int("swap", 1 - swap)
end

local terminal_rules = {
	{x= 0, y=-1, z= 0}, -- down
	{x= 1, y= 0, z= 0}, -- sideways
	{x=-1 ,y= 0, z= 0}, --
	{x= 0, y= 0, z= 1}, --
	{x= 0, y= 0, z=-1}, --
	{x= 1, y=-1, z= 0}, -- sideways + down
	{x=-1 ,y=-1, z= 0}, --
	{x= 0, y=-1, z= 1}, --
	{x= 0, y=-1, z=-1}, --
}

minetest.register_node("digiline_terminal:terminal", {
	description = "Digiline Terminal",
	groups = {choppy = 3, dig_immediate = 2},
	sounds = default and default.node_sound_stone_defaults(),
	is_ground_content = false,
	
	paramtype = "light",
	-- Maybe use this just to make all faces recieve the same light level, then slightly darken some faces of the texture.
	--light_source = 1,
	paramtype2 = "facedir",
	drawtype = "mesh",
	mesh = "digiline_terminal.obj",
	tiles = {{name = "digiline_terminal.png", backface_culling = true}},
	
	selection_box = {
		type = "fixed",
		fixed = {
			{-7/16, -8/16, -7/16, 7/16,-6.5/16, -1/16}, -- Keyboard
			{-6/16, -8/16,  0/16, 6/16,   3/16,  8/16}, -- Monitor
		}
	},
	collision_box = {
		type = "fixed",
		fixed = {
			{-7/16, -8/16, -7/16, 7/16,-6.5/16, -1/16}, -- Keyboard
			{-6/16, -8/16,  0/16, 6/16,   3/16,  8/16}, -- Monitor
		}
	},
	
	on_construct = function(pos)
		local meta = minetest.get_meta(pos)
		meta:set_string("infotext","Digiline Terminal")
		update_formspec(meta,pos)
	end,
	
	digiline = {
		receptor = {
			rules = terminal_rules,
		},
		effector = {
			rules = terminal_rules,
			action = function(pos, _, channel, message)
				local meta = minetest.get_meta(pos)
				if channel == meta:get_string("recv_channel") then
					message = digiline_terminal.to_string(message)
					-- Form feed = clear screen
					-- (Only checking at the start of the message)
					if message:sub(1,1) == "\f" then
						message = message:sub(2)
					else
						message = message.."\n"..meta:get_string("output")
					end
					meta:set_string("output", message:sub(1,1000))
					update_formspec(meta,pos)
				end
			end,
		},
	},
	on_receive_fields = function(pos, _, fields, sender)

		-- count fields array number of items
		local is_size_sup_to_1 = false
		local counter = 0
		for index in pairs(fields) do
			counter = counter + 1
			if counter > 1 then 
				is_size_sup_to_1 = true
				break
			end
		end

		local meta = minetest.get_meta(pos)
		-- quit without modif with escape
		local is_escape = not is_size_sup_to_1 and fields.quit
		if is_escape then
			-- reset entry when form is closed to avoid sensitive private cmd to be seen by everybody if recalled with last cmd button AND not executed
			meta:set_string("input","")
			update_formspec(meta,pos) 
			return 
		end

		-- vars
		local name = sender:get_player_name()
		local is_shared_use = meta:get_int("toggle_shared_use") == 1
		local is_shared_setup = meta:get_int("toggle_shared_setup") == 1

		-- ### SETTING ### (channels)
		local is_setup_modified = 	meta:get_string("send_channel") ~= fields.send_channel 
										or meta:get_string("recv_channel") ~= fields.recv_channel
		if is_setup_modified then
			if not is_shared_setup and minetest.is_protected(pos, name) then
				minetest.record_protection_violation(pos, name)
				return 
			else
				digiline_terminal.field(fields, meta, "send_channel")
				digiline_terminal.field(fields, meta, "recv_channel")
			end
		end
		if fields.key_enter_field == "send_channel"
		   		or fields.key_enter_field == "recv_channel"  then
			-- reset entry when form is closed to avoid sensitive private cmd to be seen by everybody if recalled with last cmd button AND not executed
			meta:set_string("input","")
			update_formspec(meta,pos) 
		end

		--### USE ###
		local is_using = 	fields.clear 
							or fields.key_enter_field == "input"
							or fields.send
							or fields.last
		if is_using then
			if not is_shared_use and minetest.is_protected(pos, name) then
				minetest.record_protection_violation(pos, name)
				return 
			else
				-- CLS button
				if fields.clear then
					meta:set_string("input", "")
					meta:set_string("output", "")
					update_formspec(meta,pos)
				end
				
				function send_cmd()
					digilines.receptor_send(pos, terminal_rules, fields.send_channel, fields.input)
					-- store last cmd in player meta
					local pmeta = sender:get_meta() 
					local input_to_store = fields.input
					-- avoid to big size store in meta
					if string.len(input_to_store) > 256 then
						input_to_store = string.sub(input_to_store, 1,256)
					end
					pmeta:set_string("digi_term_last",input_to_store)
					-- reset field
					meta:set_string("input", "") 
					update_formspec(meta,pos)
				end

				-- Input submitted
				if fields.key_enter_field == "input" then
					send_cmd()
				end
				
				-- Send button
				if fields.send then
					send_cmd()
				end

				-- Last command button
				if fields.last then
					-- restore last cmd from player meta
					local pmeta = sender:get_meta() 
					meta:set_string("input",pmeta:get_string("digi_term_last")) 
					update_formspec(meta,pos)
				end

			end
		end

		-- ### ADMINING ### (sharing checkboxes)
		local is_admining =  fields.toggle_shared_use and is_shared_use ~= fields.toggle_shared_use  
							or (fields.toggle_shared_setup and is_shared_setup ~= fields.toggle_shared_setup )

		if is_admining then
			if minetest.is_protected(pos, name) then
				minetest.record_protection_violation(pos, name)
				update_formspec(meta,pos)
				return 
			else
				-- Sharing checkbox
				digiline_terminal.checkbox(fields, meta, "toggle_shared_use")
				digiline_terminal.checkbox(fields, meta, "toggle_shared_setup")
				update_formspec(meta,pos)
			end
		end
	end,
})
